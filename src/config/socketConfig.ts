import path from "path";
import fse from "fs-extra";

interface IConfigJson {
  SOCKET_URI: string;
}

const initConfig: IConfigJson = {
  SOCKET_URI: "http://localhost:8000",
};

const configPath = path.resolve(__dirname, "../../client.config.json");
const configJson = readConfigJson(configPath, initConfig);

/**
 * 설정파일 읽어오기
 *
 * @param configPath 설정파일 경로
 * @param initConfig 설정 초기값
 */
function readConfigJson(configPath: string, initConfig: IConfigJson) {
  const exists = fse.existsSync(configPath);
  let configJson = initConfig;
  if (exists) {
    try {
      const json = fse.readJsonSync(configPath);

      for (let key in json) {
        (configJson as any)[key] = json[key];
      }
    } catch (err) {
      console.error(err.message);
    }
  }

  return configJson;
}

export default configJson;
