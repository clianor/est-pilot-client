import io from "socket.io-client";
import pathLib from "path";

import socketConfig from "./config/socketConfig";
import utils from "./utils";
import emitChunkedDirList from "./utils/emitChunkedDirList";

class SocketClient {
  private socket: SocketIOClient.Socket;

  constructor(uri?: string) {
    if (uri) {
      this.socket = io(uri);
    }
  }

  /**
   * 설정을 불러오기
   */
  beforeStart() {
    console.log("config: ", socketConfig.SOCKET_URI);
    if (!this.socket) {
      this.socket = io(socketConfig.SOCKET_URI);
    } else {
      console.log("이미 소켓이 생성되었습니다.");
    }
  }

  /**
   * 소켓 클라이언트 실행
   */
  run() {
    this.beforeStart();

    console.log("start client");

    this.socket.on("connect", () => {
      this.socket.emit("join", "clients");
    });

    this.socket.on("disconnect", (reason: any) => {
      console.log("disconnect reason: ", reason);
    });

    this.socket.on("connect_error", () => {
      console.log("connect_error");
    });

    this.socket.on("hello", (data: string) => {
      console.log(data);
    });

    // 디렉토리 이벤트
    this.socket.on("getDirectory", async (from: string, data: any) => {
      emitChunkedDirList(this.socket, from, data);
    });

    // 폴더만 가져오는 디렉토리 이벤트
    this.socket.on("reqFolders", async (from: string, data: any) => {
      emitChunkedDirList(this.socket, from, data);
    });

    // 파일 복사 이벤트
    this.socket.on("copyClient", async (from: string, data: any) => {
      const [ok, error] = await utils.copyClient(data);
      const parsedPath = pathLib.parse(data.fromPath);
      data["path"] = parsedPath.dir;
      emitChunkedDirList(this.socket, from, data, { ok, error });
    });

    // 파일 삭제 이벤트
    this.socket.on("deleteClient", async (from: string, data: any) => {
      const [ok, error] = await utils.deleteClient(data);
      const parsedPath = pathLib.parse(data.fromPath);
      data["path"] = parsedPath.dir;
      emitChunkedDirList(this.socket, from, data, { ok, error });
    });

    // 이름 변경 이벤트
    this.socket.on("renameClient", async (from: string, data: any) => {
      const { fromPath, toPath } = data;
      const [ok, error] = await utils.renameClient(fromPath, toPath);
      const parsedPath = pathLib.parse(fromPath);
      data["path"] = parsedPath.dir;
      emitChunkedDirList(this.socket, from, data, { ok, error });
    });

    // 파일 이동 이벤트
    this.socket.on("moveClient", async (from: string, data: any) => {
      const { fromPath, toPath } = data;
      const [ok, error] = await utils.renameClient(fromPath, toPath);
      const parsedPath = pathLib.parse(fromPath);
      data["path"] = parsedPath.dir;
      emitChunkedDirList(this.socket, from, data, { ok, error });
    });

    // 파일 업로드 이벤트
    this.socket.on("startUploadFile", (from: string, data: any) => {
      utils.file.createUploadFile(this.socket, from, data);
    });

    this.socket.on("reqUploadFile", (from: string, data: any) => {
      utils.file.reqUploadFile(this.socket, from, data);
    });

    // 파일 다운로드 이벤트
    this.socket.on("startDownloadFile", async (from: string, data: any) => {
      utils.file.downloadFile(this.socket, from, data);
    });
  }
}

const client = new SocketClient();
client.run();
