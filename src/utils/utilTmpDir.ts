import os from "os";
import pathLib from "path";

export const tmpDir = (): string => os.tmpdir();

export const joinTmpDir = (path: string): string =>
  pathLib.join(tmpDir(), path);
