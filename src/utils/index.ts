import file from "./file";

import getDriveList from "./getDriveList";
import getDirListInfo from "./getDirListInfo";
import getFileOrDirState from "./getFileOrDirState";
import renameClient from "./file/renameClient";
import copyClient from "./file/copyClient";
import deleteClient from "./file/deleteClient";

export default {
  file,
  getDriveList,
  getDirListInfo,
  getFileOrDirState,
  renameClient,
  copyClient,
  deleteClient,
};
