import child from "child_process";

const listDrivers = () => {
  return new Promise<string[]>((resolve, reject) => {
    child.exec("wmic logicaldisk get name", (error, stdout) => {
      if (error) reject(error);
      const drivers = stdout
        .split("\r\r\n")
        .filter((value) => /[A-Za-z]:/.test(value))
        .map((value) => value.trim());

      resolve(drivers);
    });
  });
};

export default listDrivers;
