import fs from "fs";
import pathLib from "path";

import listDrivers from "./getDriveList";
import getFileOrDirState from "./getFileOrDirState";

interface getDirListInfoProps {
  path: string;
  isFolder?: boolean;
}

const getDirListInfo = async ({ path, isFolder }: getDirListInfoProps) => {
  const isRoot = path === "/";
  const driveList = await (await listDrivers()).map((dirName) =>
    pathLib.join(dirName + "/")
  );

  const list = isRoot ? driveList : fs.readdirSync(path);

  const listInfo = await Promise.allSettled(
    list.map((fileName) =>
      getFileOrDirState(isRoot ? fileName : pathLib.join(path, fileName))
    )
  ).then((result) => {
    return result
      .filter((data: any) => {
        if (isFolder) {
          return data?.value?.isFolder ? true : false;
        }
        return !!data?.value;
      })
      .map((data: any) => data.value);
  });

  const result = isRoot
    ? listInfo
    : [
        {
          name: "...",
          path: driveList.includes(path) ? "/" : pathLib.resolve(path, ".."),
          ext: "folder",
          size: 0,
          isFolder: true,
          isFile: false,
        },
        ...listInfo,
      ];

  return result;
};

export default getDirListInfo;
