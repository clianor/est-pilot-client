import { chunk } from "./common";
import getDirListInfo from "./getDirListInfo";

const chunkSize = 200;

export default async function emitChunkedDirList(
  socket: SocketIOClient.Socket,
  from: string,
  data: any,
  options?: { [key: string]: any }
) {
  const dirList = await getDirListInfo(data);
  const dirListLength = dirList.length;

  if (dirListLength > chunkSize) {
    const chunkedDirList = chunk(dirList, chunkSize);
    const chunks = chunkedDirList.length;
    const firstChunk = chunkedDirList.shift();

    socket.emit(data.returnedEventStr, from, {
      success: true,
      from: socket.id,
      path: data?.path,
      directory: firstChunk,
      chunk: 1,
      chunks,
      ...options,
    });

    for (let index in chunkedDirList) {
      socket.emit(data.returnedEventStr, from, {
        success: true,
        from: socket.id,
        path: data?.path,
        directory: chunkedDirList[index],
        chunk: parseInt(index) + 2,
        chunks,
        ...options,
      });
    }
    return;
  }

  socket.emit(data.returnedEventStr, from, {
    success: true,
    from: socket.id,
    path: data?.path,
    directory: dirList,
    ...options,
  });
}
