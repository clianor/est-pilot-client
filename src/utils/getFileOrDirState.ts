import fs from "fs";
import pathLib from "path";

interface Stats {
  name?: string;
  path?: string;
  dir?: string;
  ext?: string;
  mtime?: Date;
  size?: number;
  isFolder?: boolean;
  isFile?: boolean;
  sepPath: string[];
}

const getFileOrDirState = (path: string) => {
  return new Promise<any>((resolve, reject) => {
    fs.stat(path, (err, stat) => {
      if (err) return reject(err);

      const parsedPath = pathLib.parse(path);

      const splitPath = parsedPath.dir.split(pathLib.sep);
      const splitPathLength = splitPath.length;
      const sepPath = splitPath
        .map((item: string, idx: number) => {
          if (idx < splitPathLength - 1) {
            return item + pathLib.sep;
          }
          return item;
        })
        .filter((item) => !!item);

      let info: Stats = {
        name: parsedPath.base || path,
        path: path,
        dir: parsedPath.dir,
        ext: stat.isDirectory() ? "folder" : parsedPath.ext,
        mtime: stat.mtime,
        size: stat.size,
        isFolder: stat.isDirectory(),
        isFile: stat.isFile(),
        sepPath,
      };

      resolve(info);
    });
  });
};

export default getFileOrDirState;
