import fs from "fs";
import fse from "fs-extra";

const downloadFile = async (
  socket: SocketIOClient.Socket,
  from: string,
  data: any
) => {
  fse.pathExists(data.path, (err, exists) => {
    if (err) {
      socket.emit("finishDownloadFile", from, {
        success: false,
        error: err?.message,
      });
    }

    if (exists) {
      const highWaterMark = 51200;
      const stream = fs.createReadStream(data.path, {
        highWaterMark,
      });

      stream.on("readable", () => {
        const streamData = stream.read();
        if (streamData) {
          data["data"] = streamData;
          socket.emit("reqDownloadFile", from, data);
        }
      });

      stream.on("end", () => {
        socket.emit("finishDownloadFile", from, {
          success: true,
          name: data.name,
        });
      });
    } else {
      socket.emit("finishDownloadFile", from, {
        success: false,
        error: "The file could not be found.",
      });
    }
  });
};

export default downloadFile;
