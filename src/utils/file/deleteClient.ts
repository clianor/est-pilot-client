import fse from "fs-extra";

const deleteClient = async (data: any): Promise<[boolean, Error?]> => {
  const { fromPath } = data;

  try {
    await fse.remove(fromPath);
  } catch (e) {
    return [false, e];
  }

  return [true];
};

export default deleteClient;
