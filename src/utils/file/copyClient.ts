import fs from "fs";
import fse from "fs-extra";
import pathLib from "path";

const copyClient = async (data: any): Promise<[boolean, Error?]> => {
  const { dir, name, ext } = pathLib.parse(data.fromPath);
  let count = 1;
  while (true) {
    try {
      fs.accessSync(data.fromPath, fs.constants.F_OK);
    } catch (e) {
      return [false, e];
    }

    const copyFilePath = pathLib.join(dir, name + "_COPY" + count + ext);
    const exists = await fse.pathExists(copyFilePath);

    if (exists) {
      count += 1;
    } else {
      try {
        await fse.copy(data.fromPath, copyFilePath);
      } catch (e) {
        return [false, e];
      }
      break;
    }
  }

  return [true];
};

export default copyClient;
