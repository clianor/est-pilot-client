import fs from "fs";
import fse from "fs-extra";
import path from "path";

import getDirListInfo from "../getDirListInfo";
import { joinTmpDir } from "../utilTmpDir";

const reqUploadFile = async (
  socket: SocketIOClient.Socket,
  from: string,
  data: any
) => {
  fs.appendFileSync(joinTmpDir(data.tmpName), data.data);
  delete data["data"];

  if (data.chunks > data.chunk) {
    socket.emit("reqUploadFile", from, data);
  } else {
    const exists = await fse.pathExists(path.join(data.path, data.name));
    let name;

    if (exists) {
      const parsedPath = path.parse(data.name);

      let count = 1;
      while (true) {
        name = parsedPath.name + `_${count}` + parsedPath.ext;
        const exists = await fse.pathExists(path.join(data.path, name));
        if (!exists) break;
        count++;
      }
    } else {
      name = data.name;
    }

    const oldPath: string = joinTmpDir(data.tmpName);
    const newPath: string = path.join(data.path, name);

    fse
      .move(oldPath, newPath)
      .then(async () => {
        socket.emit("finishUploadFile", from, {
          success: true,
          directory: await getDirListInfo(data),
        });
      })
      .catch((err) => {
        socket.emit("finishUploadFile", from, {
          success: false,
          error: err?.message,
        });
      });
  }
};

export default reqUploadFile;
