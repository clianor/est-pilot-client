import fs from "fs";
import fse from "fs-extra";
import { joinTmpDir } from "../utilTmpDir";

const createUploadFile = async (
  socket: SocketIOClient.Socket,
  from: string,
  data: any
) => {
  let tmpName;
  while (true) {
    tmpName = Math.random().toString(36).substr(2, 11) + ".download";
    const exists = await fse.pathExists(tmpName);
    if (!exists) {
      break;
    }
  }

  data["tmpName"] = tmpName;
  // 파일 생성을 위함
  fs.writeFileSync(joinTmpDir(tmpName), "");

  socket.emit("reqUploadFile", from, data);
};

export default createUploadFile;
