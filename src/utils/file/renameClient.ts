import fs from "fs";

const renameClient = async (
  fromPath: string,
  toPath: string
): Promise<[boolean, Error?]> => {
  try {
    await fs.renameSync(fromPath, toPath);
  } catch (e) {
    return [false, e];
  }

  return [true];
};

export default renameClient;
