import createUploadFile from "./createUploadFile";
import reqUploadFile from "./reqUploadFile";
import downloadFile from "./downloadFile";

export default {
  createUploadFile,
  reqUploadFile,
  downloadFile,
};
