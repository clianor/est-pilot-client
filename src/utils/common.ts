export function chunk(array: any[], size: number): any[] {
  const chunked = [];
  let index = 0;

  while (index < array.length) {
    const slicedArray = array.slice(index, index + size);
    chunked.push(slicedArray);
    index += size;
  }

  return chunked;
}
